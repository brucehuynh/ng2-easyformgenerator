import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UpgradeModule } from '@angular/upgrade/static';

import { AppComponent } from './app.component';

import { mainModule } from '../a1-stepway/main.js';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    mainModule,
    UpgradeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private upgrade: UpgradeModule) {
    console.log('Angular 7 is running!');
  }
  public ngDoBootstrap() {
    this.upgrade.bootstrap(document.body,
      ["eda.easyformGen.stepway"], { strictDi: true }
    );
  }
}
